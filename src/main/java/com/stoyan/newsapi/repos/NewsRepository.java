package com.stoyan.newsapi.repos;

import com.stoyan.newsapi.entities.News;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface NewsRepository extends PagingAndSortingRepository<News, Long> {

    List<News> findByDate(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @Param("date") Date date);

    List<News> findByTitle(@Param("title") String title);

    List<News> findByDateAndTitle(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @Param("date") Date date, @Param("title") String title);

}
