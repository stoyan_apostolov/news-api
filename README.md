# News API

A simple REST API for news I did for CRYPTO APIs, using Spring Data Rest and MySQL.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
An IDE of your choice, MySQL, MySQL Workbench, API client
```

### Installing

Run the database, run the following sql script, run the NewsApiApplication

```
CREATE DATABASE cooldb;

use cooldb;

create table news (
 id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, 
 date DATETIME,
 title VARCHAR(20),
 short_description VARCHAR(100),
 text TEXT
)

```

## Usage

All CRUD operations available. All requests and responses must be in JSON. For example to create news
you can use a client with POST request:

```
http://localhost:8080/news
```

With JSON body:
```
{
"date":"2002-03-27",
"title":"Lorem",
"short_description":"ipsum",
"text":"Lorem ipsum dolor sit amet."
}
```

The results are sortable ascending/descending by date or title by passing the according param:
```
curl http://localhost:8080/news?sort=date,desc
```

Can filter results by date, title and date and title:
```
curl http://localhost:8080/news/search/findByDate?date=2012-12-21
curl http://localhost:8080/news/search/findByTitle?title=RandomTitle
curl http://localhost:8080/news/search/findByDateAndTitle?date=2012-12-21&title=RandomTitle
```

## Built With

* [Spring](https://spring.io/) - The framework used
* [Gradle](https://gradle.org/) - Build tool

## Authors

* **Stoyan Apostolov** -  [GitLab](https://gitlab.com/stoyan_apostolov)
